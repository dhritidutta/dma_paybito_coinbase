const CoinbasePro = require('coinbase-pro');

var express = require("express");
var app = express();
 var bodyParser = require("body-parser");
// var router = express.Router();
 var cors = require('cors');
// const https = require('https');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ "extended": false }));
app.listen(3400, () => {
  console.log('Started in 3400');
});
const publicClient = new CoinbasePro.PublicClient();
publicClient.getProductOrderBook(
  'ETH-USD',
  { level: 3 },
  (error, response, book) => {
    /* ... */
//   console.log(book);
  }
);
const trades = publicClient.getProductTradeStream('BTC-USD', 8408000, 8409000);
//console.log(trades);
publicClient.getProductHistoricRates(
  'BTC-USD',
  { granularity: 3600 },
  (res) => {

   //       console.log(res);
  }
);
publicClient.getProduct24HrStats('BTC-USD', (error, response, data) => {
  if (error) {
    // handle the error
  } else {
    //  console.log(data);
    // work with data
  }
});
publicClient.getProductTicker('BTC-USD', (error, response, data) => {
  if (error) {
    // handle the error
  } else {
    // console.log(data);
    // work with data
  }
});
publicClient.getProductTrades('BTC-USD', (error, response, data) => {
  if (error) {
    // handle the error
  } else {
    // console.log(data);
    // work with data
  }
});
publicClient.getCurrencies((error, response, data) => {
  if (error) {
    // handle the error
  } else {
    //    console.log(data);
    // work with data
  }
});
//     const websocket = new CoinbasePro.WebsocketClient(['BTC-USD', 'ETH-USD']);

// websocket.on('message', data => {
//   /* work with data */
//   console.log(data);
// });
// websocket.on('error', err => {
//   /* handle error */
// });
// websocket.on('close', () => {
//   /* ... */
// });
const websocket = new CoinbasePro.OrderbookSync(['BTC-USD', 'ETH-USD']);
websocket.on('message', data => {
  /* work with data */
 // console.log(data);
});
websocket.on('error', err => {
  /* handle error */
});
websocket.on('close', () => {
  /* ... */
});

//   const orderbookSync = new CoinbasePro.OrderbookSync(['BTC-USD', 'ETH-USD']);
//   console.log(orderbookSync.books['ETH-USD'].state());
app.get("/public/assets", function (req, res) {
 
  publicClient.getCurrencies((error, response, data) => {
    if (error) {
      // handle the error
    } else {
    //    console.log(data);
        var allassets=data;
      // work with data
    }
    var jsonString = JSON.stringify(allassets);
    res.send(jsonString);
  });
  
 
 

})
app.get("/public/orderbooksnapshot", function (req, res) {
  const symbol = req.query.symbol;
  console.log(symbol);

  publicClient.getProductOrderBook(
    symbol,
    { level: 3 },
    (error, response, book) => {
      /* ... */
      console.log(book);
      var orderbook=book;
      var jsonString = JSON.stringify(orderbook);
    res.send(jsonString);
    }
  );
  
 
 

})